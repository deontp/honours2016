# Info Sec Terms
### Phishing
 The use of e-mails that appear to originate from a trusted source to trick a user into entering valid credentials at a fake website
### Internet
 A term to describe connecting multiple separate networks together.
### OSI
 A "reference model" for how messages should be transmitted between any two points in a network
### Integrity
 Data cannot be modified undetectably
### Worm
 A computer program that can run independently, can propagate a complete working version of itself onto other hosts on a network, and may consume computer resources destructively.
### Malware
 A generic term for a number of different types of malicious code.
### Penetrating
 Gaining unauthorized logical access to sensitive data by circumventing a system's protections
### Defacement
 Modifying the content of a website in such a way that it becomes "vandalized" or embarrassing to the website owner.
### Ethernet
 The most widely-installed LAN technology.
### Spam
 Electronic junk mail or junk newsgroup postings.
### Rootkit
 A collection of tools (programs) that a hacker uses to mask intrusion and obtain administrator-level access to a computer or computer network.
### UDP
 A common network protocol thats not TCP or ICMP
### Monoculture
 Where a large number of users run the same software, and are vulnerable to the same attacks.
### TCPDump
 A freeware console based protocol analyzer that can monitor network traffic on a wire.
### Ciphertext
 The encrypted form of the message being sent.
### Netmask
 255.255.255.0 or 255.255.248.0 or /24 or /20
### Account Harvesting
 The process of collecting all the legitimate account names on a system.
### Zero Day
 A vulnerability that is exploited before it has been disclosed
### Byte
 A fundamental unit of computer storage.
### Confidentiality
 Maintains the privacy of the people whose personal information a system holds
### Eavesdropping
 Listening to a private conversation which may reveal information.
### Fast Flux
 Protection method used by botnets consisting of a continuous and fast change of the DNS records
### Loopback
 127.0.0.1
### Availability
 The information must be available when it is needed.
### Corruption
 A threat action that undesirably alters system operation by adversely modifying system functions or data
### Virus
 A hidden, self-replicating section of computer software, usually malicious logic, that propagates by infecting
### HTTPS
This term specifies the use of HTTP enhanced by a security mechanism
### Traceroute
 A tool the maps the route a packet takes from the local machine to a remote destination.
### DDoS
 A distributed attack against accessing a service Down
